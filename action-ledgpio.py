#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import ConfigParser
from hermes_python.hermes import Hermes
from hermes_python.ontology import *
# import sys
# sys.path.append('/usr/local/lib/python2.7/dist-packages')
# from gpiozero import LED
import RPi.GPIO as GPIO
import io

CONFIGURATION_ENCODING_FORMAT = "utf-8"
# CONFIG_INI = "config.ini"

# led = LED(17)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
# you have to add _snips and _snips-skills to the group gpio

def subscribe_intent_callback(hermes, intentMessage):
    # conf = read_configuration_file(CONFIG_INI)
    # action_wrapper(hermes, intentMessage, conf)
    intentname = intentMessage.intent.intent_name
    if intentname == "FloD:LichtAn":
        if len(intentMessage.slots.Farbe) > 0:
            farbe = intentMessage.slots.Farbe.first().value # We extract the value from the slot "house_room"
            if (farbe == 'Blau' or farbe == 'Blaue' or farbe == 'Blaues')
                GPIO.output(17,True)
            result_sentence = "Schalte ein : {}".format(str(farbe))  # The response that will be said out loud by the TTS engine.
        else:
            result_sentence = "Das Licht wird eingeschaltet"
            GPIO.output(17,True)
            GPIO.output(22,True)
            GPIO.output(27,True)

        current_session_id = intentMessage.session_id
        hermes.publish_end_session(intentMessage.session_id, result_sentence)

    elif intentname == "FloD:LichtAus":
        result_sentence = "Das Licht wird ausgeschaltet"
        # led.off()
        GPIO.output(17,False)
        GPIO.output(22,False)
        GPIO.output(27,False)
        hermes.publish_end_session(intentMessage.session_id, result_sentence)



if __name__ == "__main__":
    with Hermes("localhost:1883") as h:
        h.subscribe_intents(subscribe_intent_callback).start()
